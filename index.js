const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'development';
const dbConnection = {
  development: process.env.DB_CONNECTION,
  test: process.env.DB_CONNECTION_TEST,
  staging: process.env.DB_CONNECTION,
  production   : process.env.DB_CONNECTION,
}

const isAuthenticated = require('./middlewares/auth.js');



const userRouter = require('./routers/userRouter.js');
const questionsRouter = require('./routers/questionRouter.js');
const quizRouter = require('./routers/quizRouter.js');
const answerRouter = require('./routers/answerRouter.js');

// Connect to mongodb
mongoose.connect(dbConnection[env], {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

// middleware
app.use(express.json());

app.use('/api/v1/users', userRouter);
app.use('/api/v1/questions', isAuthenticated, questionsRouter);
app.use('/api/v1/quiz', quizRouter);
app.use('/api/v1/answer', answerRouter)


app.listen(port, () => {
  console.log(`Server started at ${Date()}`)
  console.log(`Listening on port ${port}!`)
})

module.exports = app;