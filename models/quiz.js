const mongoose = require('mongoose') 
const Schema = mongoose.Schema

const quizSchema = new Schema({
  name: {
    type: 'string',
    required: true
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  questions: [{
    type: Schema.Types.ObjectId,
    ref: 'Question'
  }], 
}, {
    versionKey: false
  })
module.exports = mongoose.model('Quiz', quizSchema)