const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const questionSchema = new Schema ({
    question: {
        type: String,
        required: true
    },
    options: {
        type: Object,
        required: true
    },
    answer: {
        type: String,
        required: true
    }
}, {
    versionKey: false
})

module.exports = mongoose.model('Question', questionSchema);