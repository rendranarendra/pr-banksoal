const express = require('express');
const router = express.Router();
const isAuthenticated = require('../middlewares/auth.js');
const quizController = require('../controllers/quizController.js');
// const authAdmin = require('../middlewares/authAdmin.js');

router.post('/', isAuthenticated, quizController.answer)

module.exports = router;