const express = require('express');
const router = express.Router();
const isAuthenticated = require('../middlewares/auth');
const user = require('../controllers/userController');


router.post('/', user.create)
router.get('/', isAuthenticated, user.getAll)
router.post('/login', user.login)
router.put('/', isAuthenticated, user.update)

module.exports = router;