const express = require('express');
const router = express.Router();
const isAuthenticated = require('../middlewares/auth.js');
const question = require('../controllers/questionController.js');
const authAdmin = require('../middlewares/authAdmin.js');

router.post('/create', isAuthenticated, authAdmin, question.create)
router.get('/', isAuthenticated, question.getAll)
// router.put('/', isAuthenticated, question.update)

module.exports = router;