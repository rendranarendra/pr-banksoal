const express = require('express');
const router = express.Router();
const isAuthenticated = require('../middlewares/auth.js');
const authAdmin = require('../middlewares/authAdmin.js')
const quiz = require('../controllers/quizController.js');

router.post('/create', isAuthenticated, authAdmin, quiz.create)
router.get('/', isAuthenticated, quiz.getAll)
// router.put('/', isAuthenticated, quiz.update)

module.exports = router;