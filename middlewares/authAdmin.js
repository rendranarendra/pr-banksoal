// const user = require('../models/users.js')

async function authAdmin (req, res, next) {
    console.log(req.user)
    if (req.user.role !== 1) 
    return res.status(400).send('Access denied. You\'re not an admin!')
    next()
}

module.exports = authAdmin;
