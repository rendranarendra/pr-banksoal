const { success, error } = require("../helpers/response.js");
const Quiz = require("../models/quiz.js");
const Question = require("../models/questions.js");

exports.create = async (req, res) => {
  try {
    const quiz = await Quiz.create(req.body);
    success(res, quiz, 201);
  } catch (err) {
    error(res, err, 400);
  }
};

exports.getAll = async (req, res) => {
  try {
    const Quizes = await quiz.find();
    success(res, Quizes, 200);
  } catch (err) {
    error(res, err, 400);
  }
};

exports.answer = async (req, res) => {
  try {
    const { quiz_id, questions: answers } = req.body;
    const { questions } = await Quiz.findById(req.body.quiz_id).populate('questions')
    console.log(answers)
    let scores = 0;
    // loop question
    // dapat object
    // mencocokan pertanyaan
    // di dalam object kita bisa akses answer
    // compare jawaban user dengan kunci jawaban
    // for (let i = 0; i < questions.length; i++) {
    //   if (questions[i]._id == answer[i].question_id) {
    //     if (questions[i].answer === answers[i].answer) scores += 10;
    //   }
    // }

    for (let i = 0; i < questions.length; i++){
        if (questions[i]._id == answers[i].question_id){
            if (questions[i].answer === answers[i].answer)
            scores += 10;
        }
    }
    success(res, scores, 200)
  } catch (err) {
      error (res, err, 400)
  }
};