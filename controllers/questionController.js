const question = require('../models/questions.js')
// const user = require('../models/users.js')
const {
    success,
    error
} = require ('../helpers/response.js')


exports.getAll = async (req, res) => {
    try {
      const Questions = await question.find();
      success(res, Questions, 200)
    } catch(err) {
      error(res, err, 400)
    }
  }
  
  exports.create = async (req, res) => {
    try {
      const Question = await question.create(req.body);
      success(res, Question, 201)
      } catch(err) {
      error(res, err, 400)
    }
  }