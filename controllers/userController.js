const bcrypt = require('bcryptjs')
const User = require('../models/users.js')
const jwt = require('jsonwebtoken')
const {
    error,
    success
} = require('../helpers/response.js')

exports.getAll = async (req, res) => {
    try {
        const users = await User.find().select('-password')
        success (res, users, 200)
        } catch (err) {
        error (res, err, 400)
        }
}

exports.create = async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10)
        const hash = await bcrypt.hash(req.body.password, salt)

        // update password to hash
        req.body.password = hash

        const user = await User.create(req.body)
        success(res, user, 200)
    } catch (err) {
        error(res, err, 400)
    }
}

exports.update = async (req, res) => {
    try {
        const user = await User.findOneAndUpdate(
            { email: req.body.email },
            { $set: req.body },
            { new: true}
        );
        
        success(res, user, 200)
    } catch (err) {
        error(res, err, 400)
    }
}

exports.login = async (req, res) => {
    try {
        const token = await User.authenticate(req.body)
        success(res, token, 200)
    } catch (err) {
        error(res, err, 400)
    }
}